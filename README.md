**------- READ ME ------------**

# Introduction
Projet 1e année CNAM INFO
Sujet : Coder le jeu Asteroid en utilisant le langage C
et la bibliothèque SDL
Auteur(e) : Yassine DRESSE & Rachel ERICHER

Asteroids est un jeu video de type shoot'em up édité par Atari inc. en 1979.
Le but est de survivre le plus longtemps en tirant sur les astéroids qui
nous foncent dessus.

# Commandes 
avancer = flèche du haut
reculer = flèche du bas
aller à droite = flèche de droite
aller à gauche = flèche de gauche
se téléporter = touche Q
Tirer = espace

# Fonctionnalités 

- Vaisseau pouvant se déplacer à l'aide des touches directionnelles, 
et pouvant tirer des balles. Il peut également se téléporter 
via la touche Q, cette téléportation possède un délai de rechargement de 10s. 
A chaque collision avec un asteroid ou pendant une téléportation, le vaisseau 
est invicible pendant 2s (notifié par le clignotement).

- Génération d'astéroids dont les coordonnées sont aléatoires, et 
 qui se divise en 2 avec une réduction de la taille à chaque impact.

- Affichage du score (chaque asteroid touché rapporte 200pts) 
et du nombre de vies restantes sur l'écran.

- Lorsque le joueur ne possède plus de vies un écran de fin de partie apparaît.

# Install
Executer les commandes suivantes à la racine du projet:
```
    mkdir build
    cd ./build
    cmake ../src
``` 

# Compiler + Lancer

Executer les commandes suivantes dans le dossier build:
```
    make
    ./aster
```
